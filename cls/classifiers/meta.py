from __future__ import annotations
from enum import auto, Enum
import dataclasses
from typing import Optional, List


class MatchType(Enum):
    TERNARY = auto()
    LPM = auto()
    EXACT = auto()

    def to_json(self) -> str:
        return self.name

    @staticmethod
    def from_json(json: str) -> MatchType:
        return MatchType[json]


@dataclasses.dataclass
class TableMeta:
    path: str
    orig_path: str
    bits: Optional[List[int]]
    match_type: MatchType

    def to_json(self) -> dict:
        return {'path': self.path,
                'orig_path': self.orig_path,
                'bits': self.bits,
                'match_type': self.match_type.to_json()
                }

    @staticmethod
    def from_json(json: dict) -> TableMeta:
        return TableMeta(
                path=json['path'],
                orig_path=json['orig_path'],
                bits=json['bits'],
                match_type=MatchType.from_json(json['match_type']))


@dataclasses.dataclass
class Meta:
    groups: List[TableMeta]
    tcam: Optional[TableMeta]

    def to_json(self) -> dict:
        return {
                'groups': [g.to_json() for g in self.groups],
                'tcam': self.tcam.to_json() if self.tcam else None,
                }

    @staticmethod
    def from_json(json: dict) -> Meta:
        return Meta(
                groups=[TableMeta.from_json(g) for g in json['groups']],
                tcam=TableMeta.from_json(json['tcam']) if json['tcam'] else None,
                )
