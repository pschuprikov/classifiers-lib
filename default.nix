{ buildPythonPackage, pytest, p4t_native, numpy }:
buildPythonPackage {
  pname = "classifiers-lib";

  version = "0.0.1";

  src = ./.;

  checkPhase = ''
    python -m pytest
  '';

  buildInputs = [ pytest ];

  propagatedBuildInputs = [ p4t_native numpy ];
}
