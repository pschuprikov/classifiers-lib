{
  description = "classifier optimization library";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
  inputs.nur.url = "github:pschuprikov/nur-packages";
  inputs.nur.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, nur }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in rec {
      packages.${system} = rec {
        boost = pkgs.boost.override {
          enablePython = true;
          enableNumpy = true;
          python = pkgs.python3;
        };
        python = pkgs.python3.withPackages (pkgs:
          with pkgs; [
            pytest
            pytestrunner
            ipython
            numpy
            mypy
            pylint
            autopep8
          ]);
        p4t_native = pkgs.python3Packages.callPackage ./p4t_native { inherit boost; };
        classifiers_lib =
          pkgs.python3Packages.callPackage ./. { inherit p4t_native; };
      };
      devShells.${system} = {
        classifiers_lib = pkgs.mkShell {
          buildInputs = with self.packages.${system}; [
            boost
            python
            pkgs.pyright
          ];
          PYTHONPATH = "p4t_native/cmake-build-release/p4t/";
        };
      };
      checks.${system} = {
        inherit (packages.${system}) p4t_native classifiers_lib;
      };
    };
}
