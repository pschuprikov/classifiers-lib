{ lib, toPythonModule, clang12Stdenv, boost, cmake, catch2, tbb, python3, spdlog, fmt, ninja }:
toPythonModule (clang12Stdenv.mkDerivation {
  name = "p4t_native";
  src = ./.;
  buildInputs = [ boost catch2 tbb spdlog fmt python3 ];
  nativeBuildInputs = [ cmake ninja ];
  doCheck = true;
  cmakeFlags = [ "-DPYTHON_SITE_PACKAGES_DIR=${python3.sitePackages}" ];
  CMAKE_GENERATOR = "Ninja";
})
