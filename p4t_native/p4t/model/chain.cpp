#include "chain.h"
#include <stdexcept>

namespace p4t::model {

template <ChainKind Kind> void Chain<Kind>::push_back(Support const &support) {
    if (!chain_.empty()) {
        if constexpr (Kind == model::ChainKind::ASCENDING) {
            if (!is_subset(chain_.back(), support)) {
                throw std::invalid_argument("ascending chain violation");
            }
        } else if constexpr (Kind == model::ChainKind::DESCENDING) {
            if (!is_subset(support, chain_.back())) {
                throw std::invalid_argument("descending chain violation");
            }
        }
    }
    chain_.push_back(support);
}

template <ChainKind Kind>
Chain<Kind>::Chain(std::initializer_list<Support> ss) {
    for (auto const &s : ss) {
        push_back(s);
    }
}

template <ChainKind Kind>
auto Chain<Kind>::reversed() const -> Chain<reverse(Kind)> {
    auto result = Chain<reverse(Kind)>{};
    for (auto rit = rbegin(chain_); rit != rend(chain_); ++rit) {
        result.push_back(*rit);
    }
    return result;
}

template class Chain<ChainKind::ASCENDING>;
template class Chain<ChainKind::DESCENDING>;

}; // namespace p4t::model
