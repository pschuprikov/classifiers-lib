#ifndef RULE_HPP
#define RULE_HPP

#include <p4t/model/filter.hpp>
#include <p4t/model/rule.h>

namespace p4t {

inline auto model::operator<<(std::ostream &out, Action const &act)
        -> std::ostream & {
    return act == Action::nop() ? out << "nop" : out << act.code();
}

inline auto model::operator&(Rule const &rule, Filter::BitArray const &mask)
        -> Rule {
    return Rule(rule.filter() & mask, rule.action());
}

inline auto model::operator<<(std::ostream &out, Rule const &rule)
        -> std::ostream & {
    return out << "[" << rule.filter() << "->" << rule.action() << "]";
}

inline auto model::Rule::intersect(
        Rule const &lhs, Rule const &rhs, Filter::BitArray const &mask)
        -> bool {
    return Filter::intersect(lhs.filter(), rhs.filter(), mask) &&
           lhs.action() != rhs.action();
}

inline auto model::Rule::intersect(Rule const &lhs, Rule const &rhs) -> bool {
    return Filter::intersect(lhs.filter(), rhs.filter()) &&
           lhs.action() != rhs.action();
}

inline auto model::Rule::fast_blocker(
        Rule const &lhs, Rule const &rhs, Filter::BitArray const &mask)
        -> pair<bool, int> {
    if (lhs.action() == rhs.action()) {
        return {false, 0};
    } else {
        return Filter::fast_blocker(lhs.filter(), rhs.filter(), mask);
    }
}

} // namespace p4t

#endif
