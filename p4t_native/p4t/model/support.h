#ifndef SUPPORT_H
#define SUPPORT_H

#include <p4t/common.h>
#include <p4t/model/filter.h>

#include <boost/functional/hash.hpp>

#include <unordered_map>
#include <unordered_set>

namespace p4t::model {
using Support = vector<int>;

template <class T>
using support_map = std::unordered_map<Support, T, boost::hash<Support>>;
using support_set = std::unordered_set<Support, boost::hash<Support>>;

auto select_unique(vector<Support> supports) -> vector<Support>;

auto weight(
        vector<Support> const &unique_supports,
        vector<Support> const &all_supports) -> vector<int>;

auto select_unique_n_weight(vector<Support> const &supports)
        -> pair<vector<Support>, vector<int>>;

template <HasMask T> auto to_supports(vector<T> const &xs) -> vector<Support>;

template <HasMask T> auto to_support(T const &x) -> Support;

inline auto to_support(Filter::BitArray const &mask) -> Support;

inline auto to_support(std::tuple<int, int> const &bit_range) -> Support;

inline auto is_subset(Support const &lhs, Support const &rhs) {
    return std::includes(begin(rhs), end(rhs), begin(lhs), end(lhs));
}

inline auto is_proper_subset(Support const &lhs, Support const &rhs) {
    return is_subset(lhs, rhs) && lhs != rhs;
}

inline auto is_in_conflict(Support const &lhs, Support const &rhs) {
    return !is_subset(lhs, rhs) && !is_subset(rhs, lhs);
}

inline auto get_intersection(Support const &lhs, Support const &rhs) {
    Support result{};
    std::set_intersection(
            begin(lhs), end(lhs), begin(rhs), end(rhs), back_inserter(result));
    return result;
}

inline auto get_union(Support const &rhs, Support const &lhs) -> Support {
    Support result{};
    set_union(
            begin(rhs), end(rhs), begin(lhs), end(lhs), back_inserter(result));
    return result;
}

inline auto get_difference(Support const &rhs, Support const &lhs) -> Support {
    Support result{};
    set_difference(
            begin(rhs), end(rhs), begin(lhs), end(lhs), back_inserter(result));
    return result;
}

inline auto to_mask(Support const &support) -> Filter::BitArray;

inline auto operator<<(std::ostream &os, Support const &s) -> std::ostream &;
} // namespace p4t::model

template <> struct fmt::formatter<p4t::model::Support> {
    constexpr auto parse(format_parse_context &ctx);

    template <typename FormatContext>
    auto format(const p4t::model::Support &arr, FormatContext &ctx);
};

#endif
