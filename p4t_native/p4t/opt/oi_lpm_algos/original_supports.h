#ifndef OI_LPM_ALGOS_ORIGINAL_SUPPORTS_H
#define OI_LPM_ALGOS_ORIGINAL_SUPPORTS_H

#include <p4t/model/chain.h>
#include <p4t/model/filter.h>
#include <p4t/opt/oi_lpm_algos/common.h>

namespace p4t::opt::oi_lpm_algos {

auto find_oi_lpm_subset_original_supports(
        vector<model::Filter> const &filters,
        std::optional<int> max_support_size = std::nullopt) -> OiLpmResult;

auto find_oi_lpm_subset_original_supports(
        vector<model::Rule> const &rules,
        std::optional<int> max_support_size = std::nullopt) -> OiLpmResult;

} // namespace p4t::opt::oi_lpm_algos

#endif
