#include "transformations.h"

#include <p4t/model/filter.hpp>
#include <p4t/transformations/transformations.hpp>
#include <p4t/utils/bit_array.hpp>

#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace p4t::transformations {

template <Maskable T>
SubsetWithMask::Transformed<T>::Transformed(vector<T> const &elements)
    : indexed_filters_(elements.size()) {
    for (auto i = 0u; i < indexed_filters_.size(); i++) {
        indexed_filters_[i] = {elements[i], static_cast<int>(i)};
    }
}

template <Maskable T>
SubsetWithMask::Transformed<T>::Transformed(vector<Entry> indexed_filters)
    : indexed_filters_(std::move(indexed_filters)) {}

auto SubsetWithCommonMask::from_old(
        vector<int> const &indices, model::Support const &support)
        -> SubsetWithCommonMask {
    model::Filter::BitArray mask{};
    for (auto i : support) {
        mask.set(i, true);
    }
    return SubsetWithCommonMask(mask, indices);
}

SubsetWithCommonMask::SubsetWithCommonMask(
        model::Filter::BitArray mask, vector<int> indices)
    : indices_{indices}, mask_{mask} {}

SubsetWithMask::SubsetWithMask() = default;

SubsetWithMask::SubsetWithMask(SubsetWithCommonMask const &common_mask)
    : indices_(common_mask.indices()),
      masks_(common_mask.indices().size(), common_mask.mask()) {}

void SubsetWithMask::push_back(model::Filter::BitArray x, int index) {
    indices_.push_back(index);
    masks_.push_back(x);
}

template <Maskable T>
void SubsetWithMask::Transformed<T>::add_all(Transformed<T> const &other) {
    std::copy(
            begin(other.indexed_filters_), end(other.indexed_filters_),
            std::back_inserter(indexed_filters_));
}

template <Maskable T>
auto SubsetWithMask::Transformed<T>::empty() const -> bool {
    return indexed_filters_.empty();
}

template <Maskable T>
auto SubsetWithMask::Transformed<T>::filters() const -> Filters {
    return Filters{cbegin(indexed_filters_), cend(indexed_filters_)};
}

template <Maskable T>
auto SubsetWithMask::Transformed<T>::back() const -> T {
    return indexed_filters_.back().element;
}

template <Maskable T>
auto SubsetWithMask::Transformed<T>::size() const -> size_t {
    return indexed_filters_.size();
}

template <Maskable T>
auto SubsetWithMask::Transformed<T>::transformation() const -> SubsetWithMask {
    auto masks = vector<model::Filter::BitArray>(size());
    auto indices = vector<int>(size());
    std::transform(
            begin(indexed_filters_), end(indexed_filters_), begin(masks),
            [](auto const &x) { return x.element.mask(); });
    std::transform(
            begin(indexed_filters_), end(indexed_filters_), begin(indices),
            [](auto const &x) { return x.index; });
    return SubsetWithMask(masks, indices);
}

template <Maskable T>
void SubsetWithMask::Transformed<T>::chain(
        SubsetWithMask const &transformation) {
    auto const old_indexed_filters = std::move(indexed_filters_);

    auto iit = begin(transformation.indices());
    auto mit = begin(transformation.masks());

    for (; iit != end(transformation.indices()); ++iit, ++mit) {
        auto const &old = old_indexed_filters[*iit];
        indexed_filters_.push_back({old.element & *mit, old.index});
    }
}
template <Maskable T>
void SubsetWithMask::Transformed<T>::reverse() {
    std::reverse(begin(indexed_filters_), end(indexed_filters_));
}

template <Maskable T>
auto SubsetWithMask::Transformed<T>::extract(vector<int> const &indices)
        -> Transformed {
    auto all_indices = vector<int>(size());
    std::iota(begin(all_indices), end(all_indices), 0);
    auto remaining_indices = vector<int>(size() - indices.size());
    std::set_difference(
            begin(all_indices), end(all_indices), begin(indices), end(indices),
            begin(remaining_indices));

    auto ext_indexed_pairs = vector<Entry>(indices.size());
    std::transform(
            begin(indices), end(indices), begin(ext_indexed_pairs),
            [this](auto const i) { return indexed_filters_[i]; });
    auto const result = Transformed(std::move(ext_indexed_pairs));

    auto const old_indexed_filters = std::move(indexed_filters_);
    indexed_filters_.resize(remaining_indices.size());

    std::transform(
            begin(remaining_indices), end(remaining_indices),
            begin(indexed_filters_),
            [&old_indexed_filters](auto i) { return old_indexed_filters[i]; });

    return result;
}

template <Maskable T>
auto SubsetWithMask::Transformed<T>::pop_back() -> Entry {
    auto const entry = indexed_filters_.back();
    indexed_filters_.pop_back();
    return entry;
}

template <Maskable T>
auto SubsetWithMask::apply(vector<T> const &xs) const -> vector<T> {
    auto result = vector<T>(indices().size());
    for (auto i = 0u; i < indices_.size(); i++) {
        result[i] = xs[indices_[i]] & masks_[i];
    }
    return result;
}

template <Maskable T>
void SubsetWithMask::Transformed<T>::push_back(Entry entry) {
    indexed_filters_.push_back(std::move(entry));
}

SubsetWithMask::SubsetWithMask(
        vector<model::Filter::BitArray> masks, vector<int> indices)
    : indices_(std::move(indices)), masks_(std::move(masks)) {}

template class SubsetWithMask::Transformed<model::Filter>;
template class SubsetWithMask::Transformed<model::Rule>;
template auto SubsetWithMask::apply(vector<model::Filter> const &xs) const
        -> vector<model::Filter>;
template auto SubsetWithMask::apply(vector<model::Rule> const &xs) const
        -> vector<model::Rule>;

} // namespace p4t::transformations
