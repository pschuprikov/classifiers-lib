#include "transformations.h"

#include <p4t/model/filter.hpp>
#include <p4t/model/rule.hpp>

#include <numeric>

namespace p4t::transformations {

template <Maskable T>
template <class F>
void SubsetWithMask::Transformed<T>::apply(F f) {
    for (auto &[filter, idx] : indexed_filters_) {
        f(filter);
    }
}

template <Maskable T>
template <class F>
void SubsetWithMask::Transformed<T>::remove_if(F f) {
    auto it = std::remove_if(
            begin(indexed_filters_), end(indexed_filters_),
            [f](auto const &x) { return f(x.element); });
    indexed_filters_.erase(it, end(indexed_filters_));
}

template <Maskable T>
template <class F>
void SubsetWithMask::Transformed<T>::sort(
        F f) requires std::relation<F, T const &, T const &> {
    std::sort(
            begin(indexed_filters_), end(indexed_filters_),
            [f](auto lhs, auto rhs) {
                return f(lhs.element, rhs.element) ||
                       (!f(rhs.element, lhs.element) && lhs.index < rhs.index);
            });
}

template <Maskable T>
template <class F>
void SubsetWithMask::Transformed<T>::remove_forward_if(
        F f) requires std::relation<F, T const &, T const &> {
    auto end_it = begin(indexed_filters_);
    auto it = begin(indexed_filters_);

    for (; it != end(indexed_filters_); ++it) {
        auto covering = find_if(
                begin(indexed_filters_), end_it,
                [f, me = it->element](auto &x) { return f(x.element, me); });
        if (covering == end_it) {
            *(end_it++) = std::move(*it);
        }
    }

    indexed_filters_.erase(end_it, end(indexed_filters_));
}

template <Maskable T>
template <class F>
auto SubsetWithMask::Transformed<T>::extract_if(F f) -> Transformed {
    auto end_it = std::stable_partition(
            begin(indexed_filters_), end(indexed_filters_),
            [f](auto const &x) { return !f(x.element); });

    auto result = Transformed(vector(end_it, end(indexed_filters_)));

    indexed_filters_.erase(end_it, end(indexed_filters_));

    return result;
}

extern template class SubsetWithMask::Transformed<model::Filter>;
extern template class SubsetWithMask::Transformed<model::Rule>;
extern template auto SubsetWithMask::apply(vector<model::Filter> const& xs) const
        -> vector<model::Filter>;
extern template auto SubsetWithMask::apply(vector<model::Rule> const& xs) const
        -> vector<model::Rule>;

} // namespace p4t::transformations
