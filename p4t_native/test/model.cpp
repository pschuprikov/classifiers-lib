#include "p4t/model/support.h"
#include <catch2/catch.hpp>

#include <p4t/utils/bit_array.hpp>
#include <p4t/model/support.hpp>
#include <test/common.h>

using BitArray = p4t::utils::PackedBitArray<uint64_t, 104>;

TEST_CASE("Pop count counts fine", "[popcount]") {
    BitArray x = BitArray{};
    x.set(0, true);
    x.set(60, true);
    x.set(100, true);
    REQUIRE(BitArray::NUM_CHUNKS == 2);
    REQUIRE(p4t::utils::num::popcount(x.chunk(0)) == 2);
    REQUIRE(p4t::utils::num::popcount(x.chunk(1)) == 1);
    REQUIRE(popcount(x) == 3);
}

TEST_CASE("Non-intersection rules do not intersect", "[intersect]") {
    auto ruleA = mk_rule({"01*", 0});
    auto ruleB = mk_rule({"10*", 0});
    auto ruleC = mk_rule({"10*", 1});
    REQUIRE(Rule::intersect(ruleA, ruleB) == false);
    REQUIRE(Rule::intersect(ruleA, ruleC) == false);
}

TEST_CASE(
        "Intersecting rules with the same actions do not intersect",
        "[intersect]") {
    auto ruleA = mk_rule({"01*", 0});
    auto ruleB = mk_rule({"*11", 0});
    REQUIRE(Rule::intersect(ruleA, ruleB) == false);
}

TEST_CASE(
        "Intersecting rules with different actions do intersect",
        "[intersect]") {
    auto ruleA = mk_rule({"01*", 1});
    auto ruleB = mk_rule({"*11", 0});
    REQUIRE(Rule::intersect(ruleA, ruleB) == true);
}

TEST_CASE(
        "Pretty printing of filters works",
        "[fmt]") {
    auto filter = mk_filter("01*");
    REQUIRE(fmt::format("{}", filter) == "01*");
}

TEST_CASE(
        "Pretty printing of supports works",
        "[fmt]") {
    auto support = to_support(mk_filter("01*"));
    REQUIRE(fmt::format("{}", support) == "{0,1}");
}
