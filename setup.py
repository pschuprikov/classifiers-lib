from setuptools import setup, Extension, find_packages

try: 
    from p4t_native import min_pmgr
except ImportError:
    raise AssertionError('p4t_native module was not compiled')

setup(
    name='classifiers-lib',
    version='0.0.1',
    description='Classification framework and algorithms',
    license='Apache-2.0',
    packages=find_packages(exclude=['test*']),
    install_requires=['numpy'],
)
