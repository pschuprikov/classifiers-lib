import pytest

import cls.optimizations.range_expansion as expansion


def test_expansion_all():
    field = expansion.RangeField(0, 2 ** 16 - 1, 16)
    entry, = expansion.expand_srge(field)
    assert entry == expansion.Filter(value=[False]*16, mask=[False]*16)


def test_expansion_zero():
    field = expansion.RangeField(0, 0, 16)
    entry, = expansion.expand_srge(field)
    assert entry == expansion.Filter(value=[False]*16, mask=[True]*16)


def test_prefix_cover_single():
    assert len(expansion.prefix_cover(to_brgc(0), to_brgc(2 ** 15 - 1))) == 1


def test_cover_with_mirror_single():
    lca = to_brgc(0).lca(to_brgc(2**16 - 1))
    assert len(expansion.cover_with_mirror(to_brgc(0), to_brgc(2 ** 15 - 1), lca)) == 1

def test_mirror_full_range():
    lca = to_brgc(0).lca(to_brgc(2**16 - 1))
    assert to_brgc(0).mirror(lca) == to_brgc(2**16 - 1)

def test_leftmost():
    assert to_brgc(0).lca(to_brgc(2**16-1)).left.get_rightmost_leaf() == to_brgc(2**15 - 1)

def test_interval_size_half_all():
    assert expansion.get_interval_size(to_brgc(0), to_brgc(2**15 - 1)) == 2 ** 15
    assert expansion.get_interval_size(to_brgc(2**15), to_brgc(2**16 - 1)) == 2 ** 15

def test_lca():
    assert len(to_brgc(0).lca(to_brgc(2**16 - 1)).bits) == 0

def test_expansion_exact():
    field = expansion.RangeField(13, 13, 16)
    entry, = expansion.expand_srge(field)
    assert all(entry.mask)

def test_encoding():
    assert to_brgc_list(0) == [False] * 16
    assert to_brgc_list(1) == [False] * 15 + [True]
    assert to_brgc_list(2) == [False] * 14 + [True] + [True]
    assert to_brgc_list(3) == [False] * 14 + [True] + [False]


def test_up():
    assert list(to_brgc(1).as_node().parent.bits) == [False]*15

def test_children():
    assert to_brgc(0).as_node().parent.left == to_brgc(0).as_node()
    assert to_brgc(1).as_node().parent.right == to_brgc(1).as_node()

def test_no_next():
    with pytest.raises(ValueError):
        to_brgc(2**16 - 1).next()

def to_brgc_list(x: int):
    return list(to_brgc(x).bits)

def to_brgc(x: int):
    return expansion.BRGC.from_int(x, 16)
